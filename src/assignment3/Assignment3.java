package assignment3;
import javax.swing.JOptionPane;
import java.util.Scanner;

public class Assignment3 {
	/**
	 * A program to check a username and password
	 *
	 * @author cjp2790
	 * Date : 10/14/2014
	 * Algorithm:
	 * 1. Prompt for a choice of an account
	 * 2. Prompt for username
	 * 3. Authenticate username
	 * 4. Prompt for password
	 * 5. Authenticate password
	 * 6. Welcome user
	 */
	public static void main(String[] args) {
		
		//Initalized variables
		String rootPass = "Root";
		String adminPass = "Admin";
		String userPass = "User";
		String rootUser = "Root";
		String adminUser = "Admin";
		String nameUser = "User";
		String enteredUname;
		String enteredPass;
		int trials = 1;
		
		//Prompt for account
		Scanner keyboard = new Scanner(System.in);
		String[] choices = { "Root", "Admin", "User" };
		String input = (String) JOptionPane.showInputDialog(null,
				"Choose an account type...", "Account Type",
				JOptionPane.QUESTION_MESSAGE, null, choices, choices[1]);
			
		switch (input) {
			case "Root": {
				//Username check
				while (trials <= 3) {
					enteredUname = JOptionPane.showInputDialog("Enter Username");
					if (!rootUser.equals(enteredUname) && trials < 3) {
						JOptionPane.showMessageDialog(null, "Incorrect Username");
						trials++;
					} else if ((trials == 3)&&(!rootPass.equals(enteredUname))) {
						JOptionPane.showMessageDialog(null, "I know where you live");
						System.exit(0);
					} else
						//Password check
						while (trials <= 3) {
							enteredPass = JOptionPane.showInputDialog("Enter Password");
							if (!rootPass.equals(enteredPass) && trials < 3) {
								JOptionPane.showMessageDialog(null, "Incorrect Password");
								trials++;
							} else if ((trials == 3)&&(!adminPass.equals(enteredPass))) {
								JOptionPane.showMessageDialog(null, "I know where you live");
								System.exit(0);
							} else{
								JOptionPane.showMessageDialog(null, "Welcome " + enteredUname);
								System.exit(0);
							}
							break;
						}
				}
			}
			case "Admin": {
				//Username check
				while (trials <= 3) {
					enteredUname = JOptionPane.showInputDialog("Enter Username");
					if (!adminUser.equals(enteredUname) && trials < 3) {
						JOptionPane.showMessageDialog(null,"Incorrect Username");
						trials++;
					} else if ((trials == 3)&&(!adminPass.equals(enteredUname))) {
						JOptionPane.showMessageDialog(null, "I know where you live");
						System.exit(0);
					} else
						//Password check
						while (trials <= 3) {
							enteredPass = JOptionPane.showInputDialog("Enter Password");
							if (!adminPass.equals(enteredPass) && trials < 3) {
								JOptionPane.showMessageDialog(null,"Incorrect Password");
								trials++;
							} else if ((trials == 3)&&(!adminPass.equals(enteredPass))) {
								JOptionPane.showMessageDialog(null,"I know where you live");
								System.exit(0);
							} else{
								JOptionPane.showMessageDialog(null, "Welcome " + enteredUname);
								System.exit(0);
							}
							break;
						}
				}
			}
			case "User": {
				//Username check
				while (trials <= 3) {
					enteredUname = JOptionPane.showInputDialog("Enter Username");
					if (!nameUser.equals(enteredUname) && trials < 3) {
						JOptionPane.showMessageDialog(null,"Incorrect Username");
						trials++;
					} else if ((trials == 3)&&(!userPass.equals(enteredUname))) {
						JOptionPane.showMessageDialog(null, "I know where you live");
						System.exit(0);
					} else
						//Password check
						while (trials <= 3) {
							enteredPass = JOptionPane.showInputDialog("Enter Password");
							while (trials > 3);
							if (!userPass.equals(enteredPass) && trials < 3) {
								JOptionPane.showMessageDialog(null,"Incorrect Password");
								trials++;
							} else if ((trials == 3)&&(!userPass.equals(enteredPass))) {
								JOptionPane.showMessageDialog(null,"I know where you live");
								System.exit(0);
							} else{
								JOptionPane.showMessageDialog(null, "Welcome " + enteredUname);
								System.exit(0);
							}
							break;
						}
				}
			}
		}
	}
}
